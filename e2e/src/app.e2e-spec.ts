import { browser, logging } from 'protractor';
import { GerenciadorDeTarefasPage } from './app.po';

describe('workspace-project App', () => {
  let page: GerenciadorDeTarefasPage;

  beforeEach(() => {
    page = new GerenciadorDeTarefasPage();
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
